#
# File:   haproxyLogAnalyzer.py
# Author: Michal Swiatkowski
# E-mail: mswiatkowski@media4u.pl
#
# Created on Jan 07, 2020, 12:38:51 PM
# Modify on 15-03-2021
#
from datetime import datetime, timedelta
from collections import defaultdict
import os
import logging
import re
import sys
import collections
import configparser
import smtplib
from email.mime.text import MIMEText


class Initialize(object):
    CONFIG = "/usr/local/scr/data/haproxyLogAnalyzer.conf"
    OUTPUT = '/data/haproxy_stats'
    FILES = []
    SEND_DATA = {}

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.test_config()

    def test_config(self):
        if os.path.isfile(self.CONFIG):
            self.config.read(self.CONFIG)
            if 'DEFAULT' in self.config:
                if 'directory' in self.config['DEFAULT']:
                    directory = self.config['DEFAULT']['directory']
                    if 'files' in self.config['DEFAULT']:
                        f_names = self.config['DEFAULT']['files']
                        for file_name in f_names.split():
                            split_name = file_name.split('.')
                            file = "{}{}/{}".format(directory, split_name[0], file_name)
                            if os.path.isfile(file):
                                self.FILES.append(file)
                            else:
                                logging.error('Log file - {} - not found'.format(file))
                else:
                    logging.error('No directory in config DEFAULT section')
        else:
            logging.error('Config file not found')
            exit(1)

    def create_array_for_statistics(self):
        if self.config['DEFAULT']['admin']:
            administrators = self.config['DEFAULT']['admin'].split()
        else:
            administrators = []

        for section in self.config.sections():
            if section:
                tmp_list = []
                user_emails = self.config[section]['emails'].split()
                for email in user_emails:
                    tmp_list.append(email)

                self.SEND_DATA[section] = set(administrators + tmp_list)

    @staticmethod
    def send_email(data, status, emails):
        for email in emails:
            msg = MIMEText(data)
            msg['Subject'] = '{}'.format(status)
            msg['From'] = 'noreply@media4u.pl'
            msg['To'] = email + '@media4u.pl'

            s = smtplib.SMTP('localhost')
            s.send_message(msg)
            s.quit()


class LogPrepare(object):
    INDEX = {}
    ipRegex = '^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)'
    timeStart = 0
    timeEnd = datetime.timestamp(datetime.now())

    @staticmethod
    def read_file(input_file):
        with open(input_file) as readfile:
            for line in readfile:
                yield line

    def set_index(self, f):
        tmp_index = [{
            'INDEX': 0, 'DATE': 7, 'IP': 6, 'FRONTEND': 8, 'BACKEND': 9, 'STATUS_CODE': 11, 'USER_AGENT': 18
        }, {
            'INDEX': 1, 'DATE': 8, 'IP': 7, 'FRONTEND': 9, 'BACKEND': 10, 'STATUS_CODE': 12, 'USER_AGENT': 19
        }, {
            'INDEX': 2, 'DATE': 0, 'IP': 0, 'FRONTEND': 0, 'BACKEND': 0, 'STATUS_CODE': 0, 'USER_AGENT': 0
        }]

        stop = 0
        for index in tmp_index:
            counter = 10
            for i in self.read_file(f):
                line = i.split()
                if len(line) >= 12:
                    checks = 0
                    # check status code
                    if len(line[index['STATUS_CODE']]) == 3 and re.match('\\d{3}', line[index['STATUS_CODE']]):
                        checks += 1
                    elif line[index['STATUS_CODE']] == '-1':
                        checks += 1

                    ip = line[index['IP']].split(":")
                    if re.search(self.ipRegex, ip[0]):
                        checks += 1

                    if checks == 2:
                        counter += 1
                    else:
                        counter -= 1

                    if counter > 15:
                        # index ok, set global INDEX & brake
                        logging.info("Found index {} for file {}".format(index['INDEX'], f))
                        self.copy_dictionary(index, self.INDEX)
                        stop = 1
                        break
                    elif counter < 1:
                        # wrong index, check next
                        break

            if stop > 0:
                break

        if not self.INDEX:
            logging.error("No index for log file {}".format(f))

    @staticmethod
    def copy_dictionary(input_dict, output_dict):
        for i in input_dict:
            output_dict[i] = input_dict[i]

    def set_period(self, arr):
        default_period = '1H'
        if len(arr) > 1:
            p = arr[1]
            time_value = p[:-1]
            time_period = p[-1:]
            if time_value.isnumeric():
                period = int(time_value)
                if time_period == 'H':
                    LogPrepare.timeStart = datetime.timestamp(datetime.now() - timedelta(hours=period))
                elif time_period == 'M':
                    LogPrepare.timeStart = datetime.timestamp(datetime.now() - timedelta(minutes=period))
                else:
                    self.set_period(['', default_period])
            else:
                self.set_period(['', default_period])
        else:
            self.set_period(['', default_period])


class Buffer(LogPrepare):
    BUFFER = defaultdict(list)

    def __init__(self):
        self.insert_data_to_buffer()

    def insert_data_to_buffer(self):
        for i in LogPrepare.read_file(datafile):
            line = i.split()
            try:
                date_from_log = line[self.INDEX['DATE']]
                f = re.search('[0-9]{2}/[a-zA-Z]+/[0-9]{4}:[0-9]{2}:[0-9]{2}:[0-9]{2}', date_from_log)
                if f:
                    date_line = datetime.timestamp(datetime.strptime(f.group(), '%d/%b/%Y:%H:%M:%S'))
                    if self.timeStart <= date_line <= self.timeEnd:
                        if line[self.INDEX['FRONTEND']][-3:] == '/1:':
                            self.BUFFER['ERRORS'].append(i.rstrip('\n'))
                        else:
                            self.BUFFER[line[self.INDEX['FRONTEND']]].append(i.rstrip('\n'))
                # else:
                #     logging.error('LOG PROBLEM 1: {}'.format(i.rstrip('\n')))
            except IndexError:
                # logging.error('LOG PROBLEM 2: {}'.format(i.rstrip('\n')))
                continue

    def clear(self):
        self.INDEX.clear()
        defaultdict.clear(self.BUFFER)


class Tests(object):
    OUTPUT = []
    FILENAME = ''
    FRONT = ''
    records_number = 20
    number_ips = 800
    number_urls = 1000
    number_ua = 1000
    number_error_codes = 2

    def __init__(self, file, sent_data):

        self.FILENAME = file.split('/')[-1].split('.')[0]
        self.tmp_output_data = ''

        for frontend in list(Buffer.BUFFER):
            self.FRONT = frontend
            self.email_output = ''

            send_email = False
            for key in sent_data.keys():
                if key in frontend:
                    send_email = key

            if frontend != 'ERRORS':

                self.all_requests_count(Buffer.BUFFER[frontend])
                self.ip_count(Buffer.BUFFER[frontend], send_email)
                self.url_count(Buffer.BUFFER[frontend], send_email)
                self.user_agent_count(Buffer.BUFFER[frontend], send_email)
                self.status_code_list_by_minutes(Buffer.BUFFER[frontend], send_email)

                # contains information about IP, UA, URI
                # when tmp_output_data is not empty
                # print(self.email_output)

                if send_email and self.tmp_output_data:
                    data = '{}'.format(self.tmp_output_data)
                    status = 'HaProxy Stats: {} [{} - {}]'.format(frontend, datetime.fromtimestamp(LogPrepare.timeStart).strftime('%H:%M:%S'),
                                                                  datetime.fromtimestamp(LogPrepare.timeEnd).strftime('%H:%M:%S'))
                    Initialize.send_email(data, status, sent_data[send_email])
                    self.tmp_output_data = ''
            else:
                self.write_bad_requests(Buffer.BUFFER[frontend])

            self.save_to_file()
            del Buffer.BUFFER[frontend]

    def ip_count(self, log_list, generate_email_statistics):

        self.OUTPUT.append('####### Lista {} najczesciej powtarzajacych sie adresow IP:\n\n'.format(self.records_number))
        tmp = defaultdict(int)

        for logLine in log_list:
            ip = logLine.split()[LogPrepare.INDEX['IP']].split(":")[0]
            tmp[ip] += 1

        self.sort_and_set_data(tmp, self.records_number)

        if generate_email_statistics:
            tmp_email_output = ''
            for ip, number in tmp.items():
                if number > self.number_ips:
                    tmp_email_output += '\t{} - {}\n'.format(ip, number)

            if tmp_email_output:
                self.email_output += '## Najpopularniejsze adresy IP [powyżej {} requestów]:\n{}\n'.format(self.number_ips, tmp_email_output)

    def url_count(self, log_list, generate_email_statistics):
        self.OUTPUT.append('\n####### Lista {} najczesciej powtarzajacych sie adresow URL:\n\n'.format(self.records_number))
        tmp = defaultdict(int)

        for logLine in log_list:
            line = logLine.split()
            url = line[-2]
            protocol = line[-1]
            if url[0] == '/':
                tmp[''.join(line[-2:-1])] += 1
            else:
                if protocol[0] == '/':
                    tmp[''.join(line[-1])] += 1
                else:
                    if re.search(protocol, 'BADREQ') == -1:
                        logging.error('UrlCount log problem: {}'.format(logLine.rstrip('\n')))

        self.sort_and_set_data(tmp, self.records_number)

        if generate_email_statistics:
            tmp_email_output = ''
            for url, number in tmp.items():
                if number > self.number_urls:
                    tmp_email_output += '\t{} - {}\n'.format(url, number)

            if tmp_email_output:
                self.email_output += '## Lista adresów URL [powyżej {} requestów]:\n{}\n'.format(self.number_urls, tmp_email_output)

    def user_agent_count(self, log_list, generate_email_statistics):
        self.OUTPUT.append('\n####### Lista {} najczesciej powtarzajacych sie user agentow:\n\n'.format(self.records_number))
        tmp = defaultdict(int)
        for i in log_list:
            line = i.split()
            ua_prep = line[LogPrepare.INDEX['USER_AGENT']:-3]
            ua = ' '.join(ua_prep)
            if ua:
                if ua[0] == '{' and ua[-1] == '}':
                    text = ua.split('|')[0]
                    if text[-1] == '}':
                        user_agent = text[1:-1]
                    else:
                        user_agent = text[1:]

                    if user_agent:
                        tmp[user_agent] += 1
                    else:
                        tmp['EMPTY_USER_AGENT'] += 1

        self.sort_and_set_data(tmp, self.records_number)

        if generate_email_statistics:
            tmp_email_output = ''
            for ua, number in tmp.items():
                if number > self.number_ua:
                    tmp_email_output += '\t{} - {}\n'.format(ua, number)

            if tmp_email_output:
                self.email_output += '## Lista User-Agentów [powyzej {} requestów]:\n{}\n'.format(self.number_ua, tmp_email_output)

    def all_requests_count(self, log_list):
        self.OUTPUT.append('####### Wszystkich requestow: {}\n\n'.format(len(log_list)))

    def status_code_list_by_minutes(self, log_list, generate_email_statistics):
        self.OUTPUT.append('\n####### Lista statusow odpowiedzi serwera:\n\n')
        tmp = defaultdict(dict)
        for logLine in log_list:
            line = logLine.split()
            key = ':'.join(line[2].split(':')[:-1])
            if len(line[LogPrepare.INDEX['STATUS_CODE']]) == 3 and re.match('\\d{3}', line[LogPrepare.INDEX['STATUS_CODE']]):
                status = line[LogPrepare.INDEX['STATUS_CODE']]
                if status in tmp[key]:
                    tmp[key][status] += 1
                else:
                    tmp[key][status] = 1
            else:
                if 'wrong' in tmp[key]:
                    tmp[key]['wrong'] += 1
                else:
                    tmp[key]['wrong'] = 1

        sorted_dict = self.sort_nested_dictionary(tmp)
        tmp = ''
        for i in sorted_dict:
            tmp += '\t godzina ' + i + '\t[ '
            iter = 0
            for l in sorted_dict[i]:
                if iter == 0:
                    tmp += l + ':' + str(sorted_dict[i][l])
                else:
                    tmp += ' -- ' + l + ':' + str(sorted_dict[i][l])
                iter += 1
            tmp += ' ]\n'

        self.OUTPUT.append(tmp)

        if generate_email_statistics:
            tmp = ''
            is_error = 0
            for time in sorted_dict:
                test = ''
                for status_code, number in sorted_dict[time].items():
                    if len(status_code) == 3 and int(status_code) >= 400:
                        if number > self.number_error_codes:
                            if int(status_code) > 500:
                                is_error += 1
                            if int(status_code) > 500 and number > 10:
                                is_error += 1
                            test += '{}-{} '.format(status_code, number)
                if test:
                    tmp += '\tgodz. {}: [ {}]\n'.format(time, test)

            if tmp and is_error > 1:
                self.tmp_output_data += '## Lista błędnych kodów odpowiedzi [powyżej {} requestów]:\n{}\n'.format(self.number_error_codes, tmp)

    def write_bad_requests(self, log_list):
        self.OUTPUT.append('####### Lista blednych requestow:\n\n')
        for i in log_list:
            self.OUTPUT.append('{}\n'.format(i))

    def save_to_file(self):
        save_dir = Initialize.OUTPUT + '/' + self.FILENAME + '/'
        day = datetime.fromtimestamp(LogPrepare.timeEnd).strftime('%Y/%m/%d/')
        period = '{}--{}'.format(datetime.fromtimestamp(LogPrepare.timeEnd).strftime('%H:%M:%S'), datetime.fromtimestamp(LogPrepare.timeStart).strftime('%H:%M:%S'))
        directory = save_dir + day + period

        os.makedirs(directory, exist_ok=True)
        output = open('{}/{}'.format(directory, self.FRONT), 'w')
        for data in self.OUTPUT:
            output.write(''.join(str(data)))

        self.OUTPUT.clear()

    def sort_and_set_data(self, dictionary, number):
        sorted_list = sorted(dictionary.items(), key=lambda kv: kv[1], reverse=True)
        iterator = 1
        for i in sorted_list:
            data, count = i
            self.OUTPUT.append('\t{}. {} - {}\n'.format(iterator, data, count))
            iterator += 1

            if iterator > number:
                break

    @staticmethod
    def sort_nested_dictionary(dictionary):
        new = defaultdict(dict)
        for item in dictionary:
            tmp = dictionary[item]
            sorted_dict = collections.OrderedDict(sorted(tmp.items()))

            for i in sorted_dict:
                new[item][i] = sorted_dict[i]

        return new


if __name__ == '__main__':
    logfile = '/var/log/haproxyLogAnalyzer.log'
    formatter = '%(asctime)s %(levelname)s %(message)s'
    logging.basicConfig(filename=logfile, level=logging.DEBUG, format=formatter)

    init = Initialize()
    init.create_array_for_statistics()

    log = LogPrepare()
    log.set_period(sys.argv)

    # print('{} - {}'.format(log.timeStart, log.timeEnd))
    # print('{} - {}'.format(datetime.fromtimestamp(log.timeStart), datetime.fromtimestamp(log.timeEnd)))

    for datafile in init.FILES:
        log.set_index(datafile)
        if log.INDEX:
            buffer = Buffer()
            Tests(datafile, init.SEND_DATA)
            buffer.clear()
